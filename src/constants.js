export const ADD_POST = 'ADD_POST';
export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const SEARCH_POST = 'SEARCH_POST';
export const CLEAR_SEARCH_TEXT = 'CLEAR_SEARCH_TEXT';