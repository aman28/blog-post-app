import {
  ADD_POST,
  SHOW_MODAL,
  HIDE_MODAL,
  SEARCH_POST,
  CLEAR_SEARCH_TEXT,
} from './constants';

export const initialState = {
  posts: [
    {
      title: 'Aman Saxena',
      body: 'I have done B.E. from SISTec, Bhopal in Information Technology. Originally I belong to Shahahanpur, UP.',
      timestamp: '01/07/2020, 2:35:56',
    },
    {
      title: 'Qasim Shakir',
      body: 'I have done B.E. from SISTec, Bhopal in Information Technology. Originally I belong to Bhopal, MP.',
      timestamp: '01/07/2020, 2:35:56',
    },
  ],
  modalVisibility: false,
  searchText: '',
};

export const reducer = (state, action) => {
  switch (action.type) {
    case ADD_POST:
      const newPosts = state.posts;
      newPosts.unshift(action.payload);
      return {
        ...state,
        posts: newPosts,
      };
    case SHOW_MODAL:
      return {
        ...state,
        modalVisibility: true,
      }
    case HIDE_MODAL:
      return {
        ...state,
        modalVisibility: false,
      }
    case SEARCH_POST:
      return {
        ...state,
        searchText: action.payload,
    }
    case CLEAR_SEARCH_TEXT:
      return {
        ...state,
        searchText: '',
    }
    default:
      throw new Error();
  }
};