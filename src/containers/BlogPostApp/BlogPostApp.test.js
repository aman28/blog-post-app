import React from 'react';
import '@testing-library/jest-dom';
import {render, screen, fireEvent} from '@testing-library/react';
import BlogPostApp from '../BlogPostApp/BlogPostApp';

test('check if new post and publish buttons are visible', () => {
  render(<BlogPostApp />);

  const newPostButton = screen.getByText(/new post/i);
  const publishedButton = screen.getByText(/publish/i);

  expect(newPostButton).toBeVisible();
  expect(publishedButton).toBeVisible();
});

test('check if click on new post button, modal is visible', () => {
  render(<BlogPostApp />);

  fireEvent.click(screen.getByText(/new post/i));

  expect(screen.getByRole('modal')).toBeVisible();
});

test('on form post, post is added in the published post', () => {
  render(<BlogPostApp />);

  fireEvent.click(screen.getByText(/new post/i));

  const titleInput = screen.getByPlaceholderText('Title');
  const descriptionInput = screen.getByPlaceholderText('Description');

  const titleText = 'Football';
  const descriptionText = 'most popular game in the world';

  fireEvent.change(titleInput, {
    target: {value: titleText},
  });

  fireEvent.change(descriptionInput, {
    target: {value: descriptionText},
  });

  fireEvent.click(screen.getByRole('addPost'));

  expect(screen.getByText(titleText)).toBeInTheDocument();
  expect(screen.getByText(descriptionText)).toBeInTheDocument();
});

test('on search, correct post are visible', () => {
  render(<BlogPostApp />);

  const searchBox = screen.getByPlaceholderText('SEARCH');

  fireEvent.change(searchBox, {
    target: {value: 'Aman'},
  });

  const posts = screen.getAllByRole('post');

  for (let i = 0; i < posts.length; i++) {
    expect(posts[i]).toHaveTextContent(/Aman/i);
  }
});


