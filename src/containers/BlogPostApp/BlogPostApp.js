import React, { useReducer } from 'react';
import SearchPostInputBox from '../SearchPostInputBox/SearchPostInputBox';
import styles from './BlogPostApp.module.css';
import Button from '../../components/Button/Button';
import Post from '../../components/Post/Post';
import Modal from '../../components/Modal/Modal';
import AddNewPostForm from '../../containers/AddNewPostForm/AddNewPostForm';
import {reducer, initialState} from '../../reducer';
import { SHOW_MODAL, HIDE_MODAL } from '../../constants';

const BlogPostApp = () => {
  const [state, dispatch] = useReducer(reducer, initialState);

  const showModal = () => {
    dispatch({ type: SHOW_MODAL });
  };

  const hideModal = () => {
    dispatch({ type: HIDE_MODAL });
  };

  return (
    <div className={styles.flexContainer}>
      <div className={styles.flexItem}>
        <SearchPostInputBox dispatch={dispatch} />
      </div>
      <div className={styles.horizontalLine} />
      <div className={styles.flexItem}>
        <Button width={150} height={35} onClick={showModal} style={{ backgroundColor: '#3b5998', color: 'white' }}>
          NEW POST
        </Button>
        <Button width={150} height={35} onClick={() => console.log('published')}>
          PUBLISHED
        </Button>
      </div>
      <div className={styles.horizontalLine} />
      <div className={styles.flexItem}>
        {state.searchText !== '' ? 
          state.posts.filter(post => 
            post.title.match(new RegExp(state.searchText, 'i')) 
            || post.body.match(new RegExp(state.searchText, 'i')) 
          ).map((post, index) => 
            <Post title={post.title} body={post.body} timestamp={post.timestamp} key={index} />) 
          : state.posts.map((post, index) => <Post title={post.title} body={post.body} timestamp={post.timestamp} key={index} />)}
      </div>     
      <Modal show={state.modalVisibility} handleClose={hideModal}>
        <AddNewPostForm dispatch={dispatch} onClose={hideModal} />
      </Modal>
    </div>
  );
};

export default BlogPostApp;