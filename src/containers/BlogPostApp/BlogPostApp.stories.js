import React from 'react';
import BlogPostApp from './BlogPostApp';

export default {
  component: BlogPostApp,
  title: 'BlogPostApp',
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const basicBlogPostApp = () => <BlogPostApp />;
    

