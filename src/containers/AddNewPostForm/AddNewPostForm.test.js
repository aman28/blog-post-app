import React from 'react';
import '@testing-library/jest-dom';
import {render, screen, fireEvent} from '@testing-library/react';
import AddNewPostForm from '../AddNewPostForm/AddNewPostForm';
import { ADD_POST } from '../../constants';

test('dispatch is triggered on publish button click in add new post form with correct values', () => {
  const dispatch = jest.fn();
  const onClose = jest.fn();
  render(<AddNewPostForm dispatch={dispatch} onClose={onClose} />);

  const titleInput = screen.getByPlaceholderText('Title');
  const descriptionInput = screen.getByPlaceholderText('Description');

  const titleText = 'Football';
  const descriptionText = 'most popular game in the world';

  fireEvent.change(titleInput, {
    target: {value: titleText},
  });

  fireEvent.change(descriptionInput, {
    target: {value: descriptionText},
  });

  fireEvent.click(screen.getByRole('addPost'));

  expect(dispatch).toHaveBeenCalled();
  expect(dispatch).toHaveBeenCalledWith({
    type: ADD_POST, payload: { 
      title: titleText, 
      body: descriptionText, 
      timestamp: new Date().toLocaleString('en-GB'), 
    }},
  );
});


