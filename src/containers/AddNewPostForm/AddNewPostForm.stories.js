import React from 'react';
import AddNewPostForm from './AddNewPostForm';

export default {
  component: AddNewPostForm,
  title: 'AddNewPostForm',
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const basicAddNewPostForm = () => <AddNewPostForm />;
    

