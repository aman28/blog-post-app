/* eslint-disable jsx-a11y/aria-role */
import React, { useState } from 'react';
import styles from './AddNewPostForm.module.css';
import PropTypes from 'prop-types';
import { ADD_POST } from '../../constants';

const AddNewPostForm = ({ dispatch, onClose }) => {
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');

  return (
    <div className={styles.formContainer}>
      <div className={styles.formItem}>
        <label className={styles.block}>Title</label>
        <input 
          type="text" 
          placeholder="Title"
          value={title} 
          onChange={event => setTitle(event.target.value)} 
          className={styles.titleInput} 
          style={{ width: '93%', height: '20px' }}
        />
      </div>
      <div className={styles.formItem}>
        <label className={styles.block}>Description</label>
        <textarea 
          placeholder="Description"
          value={body} 
          onChange={event => setBody(event.target.value)} 
          className={styles.textarea} 
        />
      </div>
      <div className={styles.buttonContainer}>
        <input
         role="addPost"
         type="button" 
         value="PUBLISH"
         className={`${styles.submitButton} ${title === '' || body === '' ? styles.disable : ''}`}
          onClick={() => {
            if (title === '' || body === '') return;
            dispatch({type: ADD_POST, payload: { title: title, body: body, timestamp: new Date().toLocaleString('en-GB') }});
            onClose();
          }}
        />
      </div>
    </div>
  );
};

AddNewPostForm.propTypes = {
  dispatch: PropTypes.func,
  onClose: PropTypes.func,
};

export default AddNewPostForm;