import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './SearchPostInputBox.module.css';
import searchIcon from '../../images/search.svg';
import cancelIcon from '../../images/cancel.svg';
import { SEARCH_POST, CLEAR_SEARCH_TEXT } from '../../constants';

const SearchPostInputBox = ({ dispatch }) => {
  const [searchText, setSearchText] = useState('');

  const escapeRegExp = string => string.replace(/[.*+\-?^${}()|[\]\\]/g, '\\$&');

  return (
    <div className={styles.inputContainer}>
      <img 
        src={searchIcon} 
        alt="searchIcon"
        className={styles.icon}
      />
      <form style={{ display: 'inline-block' }} onSubmit={event => {
          event.preventDefault();
          dispatch({ type: SEARCH_POST, payload: searchText });
        }}
      >
        <input 
          type="text" 
          placeholder="SEARCH" 
          className={styles.input}
          value={searchText} 
          onChange={event => { 
            setSearchText(event.target.value); 
            dispatch({ type: SEARCH_POST, payload: escapeRegExp(event.target.value) });
          }}
        />
      </form>
      <div className={styles.verticalDivider} />
      <img 
        src={cancelIcon} 
        alt="cancelIcon"
        className={styles.icon}
        onClick={() => { 
          setSearchText('');
          dispatch({ type: CLEAR_SEARCH_TEXT }); 
        }}
      />
    </div>
  );
};

SearchPostInputBox.propTypes = {
  dispatch: PropTypes.func,
};

export default SearchPostInputBox;