import React from 'react';
import SearchPostInputBox from './SearchPostInputBox';

export default {
  component: SearchPostInputBox,
  title: 'SearchPostInputBox',
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const searchPostInputBox = () => <SearchPostInputBox />;
    

