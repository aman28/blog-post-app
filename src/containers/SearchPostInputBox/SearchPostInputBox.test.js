import React from 'react';
import '@testing-library/jest-dom';
import {render, screen, fireEvent} from '@testing-library/react';
import BlogPostApp from '../BlogPostApp/BlogPostApp';
import  SearchPostInputBox from './SearchPostInputBox';
import { SEARCH_POST, CLEAR_SEARCH_TEXT } from '../../constants';

test('on clear the textbox value is empty', () => {
  render(<BlogPostApp />);

  const searchBox = screen.getByPlaceholderText('SEARCH');

  fireEvent.change(searchBox, {
    target: {value: 'Aman'},
  });

  fireEvent.click(screen.getByAltText('cancelIcon'));

  expect(screen.getByPlaceholderText('SEARCH')).toHaveValue('');
});

test('dispatch is triggered on change of input value with correct values', () => {
  const dispatch = jest.fn();
  render(<SearchPostInputBox dispatch={dispatch}/>);

  const searchBox = screen.getByPlaceholderText('SEARCH');
  const value = 'Aman';
  fireEvent.change(searchBox, {
    target: {value},
  });

  expect(dispatch).toHaveBeenCalled();
  expect(dispatch).toHaveBeenCalledWith({type: SEARCH_POST, payload: value});
});

test('dispatch is triggered on cancel click', () => {
  const dispatch = jest.fn();
  render(<SearchPostInputBox dispatch={dispatch}/>);
  fireEvent.click(screen.getByAltText('cancelIcon'));
  expect(dispatch).toHaveBeenCalled();
  expect(dispatch).toHaveBeenCalledWith({type: CLEAR_SEARCH_TEXT});
});