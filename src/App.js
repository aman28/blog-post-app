import React from 'react';
import BlogPostApp from './containers/BlogPostApp/BlogPostApp';

function App() {
  return (
    <div>
      <BlogPostApp />
    </div>
  );
}

export default App;
