/* eslint-disable jsx-a11y/aria-role */
import React from 'react';
import PropTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';
import styles from './Post.module.css';

const Post = ({ title, body, timestamp }) => {
  return (
    <div className={styles.post} role="post">
      <div className={styles.flexContainer}>
        <div className={styles.title}>
          {title}
        </div>
        <div className={styles.timestamp}>
          {timestamp}
        </div>
      </div>
      <div className={styles.body}>
        <ReactMarkdown source={body} />
      </div>
    </div>
  );
};

Post.propTypes = {
  title: PropTypes.string.isRequired,
  body: PropTypes.string.isRequired,
  timestamp: PropTypes.string.isRequired,
};

export default Post;