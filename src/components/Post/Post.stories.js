import React from 'react';
import { withKnobs, text } from '@storybook/addon-knobs/react';
import Post from './Post';

export default {
  component: Post,
  title: 'Post',
  decorators: [withKnobs],
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const demoPost = () => <Post
    title={text('title', 'Aman Saxena')}
    body={text('body', 'B.E. from SISTec, Bhopal and belongs to Shahjahanpur, UP, interested in football and Cricket')}
    timestamp={text('timestamp', '01/07/2020, 2:35:56')}
  />;
