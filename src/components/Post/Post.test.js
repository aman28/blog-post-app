import React from 'react';
import '@testing-library/jest-dom';
import {render, screen } from '@testing-library/react';
import Post from './Post';

test('all the data passed in props to Post component is visible', () => {
  render(<Post title="Aman" body="Shahjahanpur, UP" timestamp="01/07/2020, 2:35:56" />);

  expect(screen.getByText('Aman')).toBeVisible();
  expect(screen.getByText('Shahjahanpur, UP')).toBeVisible();
  expect(screen.getByText('01/07/2020, 2:35:56')).toBeVisible();
});


