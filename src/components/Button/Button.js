import React from 'react';
import styles from './Button.module.css';
import PropTypes from 'prop-types';

const Button = ({ children, width, height, onClick, style }) => { 
  return (
    <button 
      className={styles.button}
      style={{ 
        width: `${width}px`, 
        height: `${height}px`, 
        ...style,
      }}
      onClick={() => onClick()}
    >
      {children}
    </button>
  );
};

Button.propTypes = {
  children: PropTypes.string.isRequired,
  width: PropTypes.number,
  height: PropTypes.number,
  onClick: PropTypes.func,
  style: PropTypes.object,
};

export default Button;