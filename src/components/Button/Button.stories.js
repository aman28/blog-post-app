import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, number } from '@storybook/addon-knobs/react';
import Button from './Button';

export default {
  component: Button,
  title: 'Button',
  decorators: [withKnobs],
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const ButtonWithText = () => <Button>NEW POST</Button>;
  
export const setButtonWidthAndHeight =  () => <Button
    width={number('width', 150)}
    height={number('height', 50)}
  >
    PUBLISHED
  </Button>;

export const clickButton = () => <Button
    onClick={action('onClick')}
  >
    NEW POST
  </Button>;

