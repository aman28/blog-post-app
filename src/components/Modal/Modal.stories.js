import React from 'react';
import { action } from '@storybook/addon-actions';
import { withKnobs, boolean } from '@storybook/addon-knobs/react';
import Modal from './Modal';

export default {
  component: Modal,
  title: 'Modal',
  decorators: [withKnobs],
  // Our exports that end in "Data" are not stories.
  excludeStories: /.*Data$/,
};

export const ModalWithText = () => <Modal show={true}>Modal</Modal>;
  
export const toggleModalVisibility =  () => <Modal
    show={boolean('show', false)}
  />;

export const closeModal = () => <Modal
    show={true}
    handleClose={action('handleClose')}
  />;
