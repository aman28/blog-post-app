/* eslint-disable jsx-a11y/aria-role */
import React from 'react';
import PropTypes from 'prop-types';
import styles from './Modal.module.css';
import closeIcon from '../../images/close.svg';

const Modal = ({ show, children, handleClose }) => {
  return show ? (
    <div role="modal" className={styles.modal} onClick={handleClose}>
      <section className={styles.modalMain} onClick={event => event.stopPropagation()}>
        <img src={closeIcon} alt="closeModal" className={styles.closeIcon} onClick={handleClose} />
        {children}
      </section>
    </div>
  ) : null;
};

Modal.propTypes = {
  show: PropTypes.bool,
  children: PropTypes.object,
  handleClose: PropTypes.func.isRequired,
};

export default Modal;