# Tests

## Unit Tests

### Search Component

* Input from the text box is going properly to the parent component (Done)

* On clear the textbox value is empty (Done) (Fixed)

### Form Component

* On submit value of all fields are sent (Done)

### Main Component

* Both the buttons are visible  (Done) (Fixed)

* on click of Post form modal is visible (Done)

### Published posts

* All the data passed in props are visible (Done)

## Integration Tests

* On search correct post are visible (Done)

* On Form post that post is added in the published post (Done)
